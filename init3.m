clc
clear
close all

r_ball = 0.12;
h_ball0 = 1.8;
h_ball_after_bounce_u = 1.4;
h_ball_after_bounce_b = 1.2;
m_ball = 0.6;

ball_stiffness = 1e6;
ball_damping = 70;



r_tball = 0.065/2;
h_tball0 = 2.54;
h_tball_after_bounce_u = 1.47;
h_tball_after_bounce_b = 1.35;
m_tball = 0.0585;


tball_stiffness = 1e6;
tball_damping = 45;


ball_tball_stiffness = 1e6/2;
ball_tball_damping = ball_damping+tball_damping;
